//
//  PictureFeedCollectionViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class PictureFeedCollectionViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout {

    var arrayResult : NSMutableArray!
    var shouldEndPage = Bool()
    var pageIndex : Int = 1
    
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 3,
        minimumLineSpacing: 3,
        sectionInset: UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Register cell classes

        collectionView?.collectionViewLayout = columnLayout
        if #available(iOS 11.0, *) {
            collectionView?.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }
        
        //self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.getFlowerList(pageIndex: pageIndex)
    }

    
    // MARK: Seervice Call

    func getFlowerList(pageIndex : Int) {
        
        let urlString = "https://pixabay.com/api/?key=10961674-bf47eb00b05f514cdd08f6e11&q=flower&page=\(pageIndex)"
        let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: escapedString!) else{
            return
        }
        
        IndicatorHUD.shared.showHUD(message: "Please wait...")
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let _ = data,error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return
            }
            do {
                // data we are getting from network request
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let response = try decoder.decode(Flower.self, from: data!)
               
                self.pageIndex = pageIndex
                
                if pageIndex == 3{
                   self.shouldEndPage = true
                }
                
                if self.arrayResult == nil || self.arrayResult.count == 0 && pageIndex == 0{
                    self.arrayResult = NSMutableArray.init(array: response.hits)
                }else{
                   self.arrayResult.addObjects(from: response.hits)
                }
                
                DispatchQueue.main.async {
                    IndicatorHUD.shared.dismissGlobalHUD()
                    self.collectionView.reloadData()
                }
                
                //print(response) //Output - EMT
                
            } catch { print(error)
                DispatchQueue.main.async {
                    IndicatorHUD.shared.dismissGlobalHUD()
                }
            }
            
         }
        task.resume()
        
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.arrayResult?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if self.arrayResult.count > 0 && indexPath.row == self.arrayResult.count - 1 && self.shouldEndPage == false{
            self.getFlowerList(pageIndex: pageIndex + 1)
        }
    }
    

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        if let model = self.arrayResult.object(at: indexPath.row) as? Hit{
            
            if let imageView = cell.viewWithTag(1001) as? UIImageView{
                imageView.loadImageUsingCache(withUrl: model.previewURL!)
            }
        }
        
        // Configure the cell
        
    
        return cell
    }


    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}


let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
 
    func loadImageUsingCache(withUrl urlString : String) {
       
        guard let url = URL(string: urlString) else{
            return
        }
        
        self.image = nil
        
        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = cachedImage
            return
        }
        
        // if not, download image from url
        
        let activity = UIActivityIndicatorView.init(frame: self.frame)
        activity.style = .gray
        activity.hidesWhenStopped = true
        activity.startAnimating()
        self.addSubview(activity)
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                    activity.stopAnimating()

                }
            }
            
        }).resume()
    }
}
