//
//  VideoPreviewViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit

class VideoPreviewViewController: UIViewController {

    @IBOutlet weak var objCustomPlayer: CustomPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name("StopVideo"), object: nil)
    }
    
    
    func startPlay() {
        //optional method
        objCustomPlayer.startPlayback()
    }
    
    func stopPlay() {
        //optional method
        objCustomPlayer.pausePlayback()
    }
    


}
